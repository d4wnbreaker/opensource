/*
    Code by Lucas Perez (@d4wnbreaker)
    Dungeons and Dragons 5th Edition is a Role Playing Game
    by Wizards of the Coast

    What is coming (order of release):
    -
    - Better written main method (I know, it's very messy at the moment)
    - Installer for Windows
    - Android App (maybe)
    - Installer for Macs (maybe)
*/

import java.util.Scanner;
import java.util.stream.*;

public class DnD5e
{
    public static void main (String[] args)
    {
        Scanner userInput = new Scanner(System.in);
        XPDistribution xp = new XPDistribution();
        MonsterMultiplier xpMultiplier = new MonsterMultiplier();
        int partySize; int playerLevel; int encounterDifficulty; int characterCount = 1;
        int monsterNumber; int monsterXP = 0; int monsterCount = 1; double finalXP; int sum;

        System.out.println("Enter the amount of members on your party:");
        partySize = userInput.nextInt();

        int[] array = new int[partySize];

        for (int i = 0; i <= partySize-1; i++)
        {
            System.out.println("Enter the level of the character " + characterCount + ":");
            playerLevel = userInput.nextInt();
            array[i] = playerLevel;
            characterCount++;
        }

        System.out.println("How many enemies in the encounter?");
        monsterNumber = userInput.nextInt();

        for (int i = 1; i <= monsterNumber; i++)
        {
            System.out.println("Enter the XP provided by the monster " + monsterCount + ":");
            monsterXP = userInput.nextInt() + monsterXP;
            monsterCount++;
        }

        int[] xpEasy = new int[partySize];
        int[] xpMedium = new int[partySize];
        int[] xpHard = new int[partySize];
        int[] xpDeadly = new int[partySize];

        for (int i = 0; i < xpEasy.length; i++)
        {
            xpEasy[i] = xp.getSlot(array[i], 1);
            xpMedium[i] = xp.getSlot(array[i], 2);
            xpHard[i] = xp.getSlot(array[i], 3);
            xpDeadly[i] = xp.getSlot(array[i], 4);
        }

        System.out.println();
        System.out.print("Easy: ");
        sum = IntStream.of(xpEasy).sum();
        System.out.print(sum + " XP " + "(");
        for (int j: xpEasy)
        {
            System.out.print(" " + j + " ");
        }
        System.out.print(")");

        System.out.println();
        System.out.print("Medium: " );
        sum = IntStream.of(xpMedium).sum();
        System.out.print(sum + " XP " + "(");
        for (int j: xpMedium)
        {
            System.out.print(" " + j + " ");
        }
        System.out.print(")");

        System.out.println();
        System.out.print("Hard: ");
        sum = IntStream.of(xpHard).sum();
        System.out.print(sum + " XP " + "(");
        for (int j: xpHard)
        {
            System.out.print(" " + j + " ");
        }
        System.out.print(")");

        System.out.println();
        System.out.print("Deadly: ");
        sum = IntStream.of(xpDeadly).sum();
        System.out.print(sum + " XP " + "(");
        for (int j: xpDeadly)
        {
            System.out.print(" " + j + " ");
        }
        System.out.print(")");

        System.out.println();
        System.out.println("The considered XP Threshold for the encounter is: " + (xpMultiplier.getMonsterMultiplier(monsterNumber, monsterXP) /1.0 ) + " XP.");
        System.out.println("Every adventurer receives " + (monsterXP / partySize) + " XP ");

        /*
        for (int i = 0; i < xp.xpTableArray.length; i++)
        {
            for (int j = 0; j < xp.xpTableArray[i].length; j++)
            {
                // do bubble sort with monsterXP until finding a result that is equal or just a little above one value
            }
        } */
    }
}
