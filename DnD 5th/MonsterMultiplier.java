public class MonsterMultiplier
{
    private double multiplier; private int monsterNumber; private double monsterEXP; private int partySize;

    public int getPartySize(int newPartySize)
    {
        partySize = newPartySize;
        return partySize;
    }

    public double getMonsterMultiplier(int newMonsterNumber, double newMonsterEXP)
    {
        monsterNumber = newMonsterNumber;
        monsterEXP = newMonsterEXP;

        if (monsterNumber == 1)
        {
            multiplier = monsterEXP * 1;
        }
        else if (monsterNumber == 2)
        {
            multiplier = monsterEXP * 1.5;
        }
        else if (monsterNumber > 2 && monsterNumber < 7)
        {
            multiplier = monsterEXP * 2;
        }
        else if (monsterNumber > 6 && monsterNumber < 11)
        {
            multiplier = monsterEXP * 2.5;
        }
        else if (monsterNumber > 10 && monsterNumber < 15)
        {
            multiplier = monsterEXP * 3;
        }
        else if (monsterNumber > 15)
        {
            multiplier = monsterEXP * 4;
        }
        else
        {
            multiplier = monsterEXP;
        }
        return multiplier;
    }
}
