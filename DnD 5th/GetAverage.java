public class GetAverage
{
    private int partySize;
    private int playerLevel;
    private int partyAverage;

    public int setPartySize(int newPartySize)
    {
        partySize = newPartySize;
        return partySize;
    }

    public int setPlayerLevel(int newPlayerLevel)
    {
        playerLevel = newPlayerLevel + playerLevel;
        return playerLevel;
    }

    public int getAverage()
    {
        partyAverage = playerLevel / partySize;
        return partyAverage;
    }

    public void printAverage()
    {
        System.out.println(partyAverage);
    }
}
